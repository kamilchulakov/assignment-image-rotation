#include "../include/file_io.h"
#include <stdio.h>
#include <stdlib.h>

extern FILE* open_read(const char* path) {
    return fopen(path, "rb");
}

extern FILE* open_write(const char* path) {
    return fopen(path, "wb");
}

extern enum io_status close(FILE* file) {
    if (fclose(file) == 0) {
        return OK;
    }
    return BAD;
}
