#include "../include/bmp.h"
#include  <stdint.h>
#include <stdlib.h>
// Если ширина изображения в байтах кратна четырём, то строчки идут одна за другой без пропусков.
// Если ширина не кратна четырём, то она дополняется мусорными байтами до ближайшего числа, кратного четырём.
// Эти байты называются padding.
static uint8_t calc_padding(const size_t width) {
    return DWORD_SIZE - (width * sizeof(struct pixel)) % DWORD_SIZE;
}

static void create_header(struct bmp_header* header, const size_t width, const size_t height) {
    const uint8_t padding = calc_padding(width);
    const size_t image_size = (sizeof(struct pixel) * width + padding) * height;
    header->bfType = BMP_TYPE;
    header->biBitCount = BMP_VALID_BIT_COUNT;
    header->biXPelsPerMeter = BMP_PIXEL_PER_METER;
    header->biYPelsPerMeter = BMP_PIXEL_PER_METER;
    header->bfileSize = image_size + sizeof(struct bmp_header);
    header->bfReserved = BMP_RESERVED;
    header->bOffBits = sizeof(struct bmp_header);
    header->biSize = BMP_HEADER_SIZE;
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP_PLANES;
    header->biCompression = BMP_COMPRESSION;
    header->biSizeImage = image_size;
    header->biClrUsed = BMP_CIR_USED;
    header->biClrImportant = BMP_CIR_IMPORTANT;
}

extern enum read_status from_bmp( FILE const* in, struct image* img) {
    // read bmp_header
    const struct bmp_header header;

    if (fread((struct bmp_header*) &header, sizeof(struct bmp_header), 1, (FILE *) in) != 1 || header.bfType != BMP_TYPE)
        return READ_INVALID_SIGNATURE;
    if (header.biBitCount != BMP_VALID_BIT_COUNT)
        return READ_INVALID_HEADER;


    const size_t width = img -> width = header.biWidth;
    const size_t height = img -> height = header.biHeight;

    // read image (only pixels are needed)
    struct pixel* pixels = malloc(width * height * sizeof(struct pixel));

    if (pixels == NULL) return READ_OUT_OF_MEMORY;

    if (fseek((FILE *) in, header.bOffBits, SEEK_SET) != 0) {
        free(pixels);
        return READ_INVALID_HEADER;
    }

    uint8_t padding = calc_padding(img->width);
    for (size_t i = 0; i < height; i++) {
        if (fread(pixels + i * width, width * sizeof(struct pixel), 1, (FILE *) in) != 1){
            free(pixels);
            return READ_INVALID_BITS;
        }
        if (fseek((FILE *) in, padding, SEEK_CUR) != 0) {
            free(pixels);
            return READ_INVALID_BITS;
        }
    }

    // save read pixels
    img->data = pixels;
    return READ_OK;
}
extern enum write_status to_bmp( FILE const* out, struct image const* img ) {
    if (out == NULL || img == NULL || img->data == NULL) return WRITE_ERROR;

    // make header
    const struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if (header == NULL) return WRITE_ERROR;
    create_header((struct bmp_header*) header, img->width, img->height);

    // write header
    if (fwrite(header, sizeof(struct bmp_header), 1, (FILE *)out) != 1) {
        free((struct bmp_header*) header);
        return WRITE_ERROR;
    }

    if (fseek((FILE *)out, header->bOffBits, SEEK_SET) != 0) {
        free((struct bmp_header*) header);
        return WRITE_ERROR;
    }

    // write image
    const size_t width = img->width;
    const size_t height = img->height;
    const uint8_t padding = calc_padding(width);
    uint8_t* null_bits = calloc(1, padding);
    if (null_bits == NULL) {
        free((struct bmp_header*) header);
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < height; i++) {
        if (fwrite(img->data + width * i, width * sizeof(struct pixel),1, (FILE *)out) != 1 ) {
            free((struct bmp_header*) header);
            free(null_bits);
            return WRITE_ERROR;
        }
        if (fwrite(null_bits, padding, 1, (FILE *) out) != 1) {
            free((struct bmp_header*) header);
            free(null_bits);
            return WRITE_ERROR;
        }
    }

    // free(dom)
    free((struct bmp_header*) header);
    free(null_bits);

    return WRITE_OK;
}
