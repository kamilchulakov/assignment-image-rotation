#include "../include/transformations.h"
extern struct image* rotated( struct image const* source ) {
    if (source == NULL) {
        fprintf( stderr, "Bad source image...\n" );
        return NULL;
    }

    const size_t height = source->height;
    const size_t width = source->width;

    struct image* img = image_create(height, width);
    if (img == NULL) return NULL;
    img->data = malloc(sizeof(struct pixel) * img->height * img->width);
    if (img->data == NULL) return NULL;
    for (size_t i = 0; i < height; i++) {
        for (size_t j = 0; j < width; j++) {
            img->data[height - i - 1 + j * height] = source->data[i * width + j];
        }
    }
    return (struct image*) img;
}
