#include "file_io.h"
#include "bmp.h"
#include "image.h"
#include "transformations.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv ) {
    // THIS FILE (MAIN) PARTS:

    const char* read_errors[] = {
            [READ_INVALID_SIGNATURE] = "Error occurred:\nInvalid signature.\n",
            [READ_INVALID_BITS] = "Error occurred:\nInvalid pixel data.\n",
            [READ_INVALID_HEADER] = "Error occurred:\nInvalid header.\n"
    };

    const char* write_errors[] = {
            [WRITE_ERROR] = "Error occurred during writing into file.\n"
    };

    // INPUT STUFF:
    // get 2 strings or fail

    if (argc != 3) {
        print_err("Provide 2 args!\n1 arg for input filename;\n1 arg for output filename.\n");
        return -1;
    }
    char *input = argv[1];
    char *output = argv[2];
    printf("Got input filename: %s\n", input);
    printf("Got output filename: %s\n", output);

    // READ:
    // open input (file_io stuff file) = get file (link) from (path)
    // read input as bmp (bmp specific -> in bmp file) = get image (link) from (file link)
    printf("Reading...\n");
    FILE* in = open_read(input);
    if (in == NULL) {
        print_err("No such file:\n");
        print_err(input);
        print_err("\n");
        return -1;
    }
    struct image* img = image_create(0, 0);
    if (img == NULL) {
        print_err("Memory wasn't allocated for image!\n");
        return -1;
    }
    enum read_status status = from_bmp(in, img);
    close(in);
    if (status != READ_OK) {
        print_err(read_errors[status]);
        image_free(img);
        return -1;
    }

    // TRANSFORM:
    // transform (transformations file -> rotate) = get modified image (link) from (image link)
    printf("Transforming...\n");
    struct image* new_img = rotated(img);
    image_free(img);
    if (new_img == NULL || new_img->data == NULL) {
        print_err("Bad new image!\n");
        return -1;
    }

    // SAVE:
    // open output (file_io stuff file) = get file (link) from (path)
    // write output as bmp (bmp specific -> in bmp file) = save image (real file) from (file link, image link)
    FILE* out = open_write(output);
    enum write_status w_status = to_bmp(out, new_img);
    image_free(new_img);
    close(out);
    if (w_status != WRITE_OK) {
        print_err(write_errors[w_status]);
        return -1;
    }
    printf("Done...\n");
    return 0;

    // FILES:

    // FILE_IO:
    // open file to read, open file to write

    // BMP:
    // read file as bmp to get image:
    // read file with bmp_header, read image, return image
    // read image from file (BMP skipped header and calls this)
    // save image to file (BMP skipped header and calls this)

    // TRANSFORM
    // struct image rotate( struct image const source );

    // IMAGE:
    // hold image struct

}
