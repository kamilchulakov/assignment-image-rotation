#include "../include/image.h"

extern struct image* image_create( size_t width, size_t height ) {
    struct image* img = malloc(sizeof(struct image));
    if (img) {
        img->data = NULL;
        img->width = width;
        img->height = height;
    }
    return img;
}

extern void image_free(struct image* img) {
    if (img) {
        if (img->data) free(img->data);
        free(img);
    }
}
