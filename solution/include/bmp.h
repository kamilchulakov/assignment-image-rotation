#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H
#include "image.h"
#include <inttypes.h>
#include <stdio.h>

#define BMP_TYPE 0x4D42
#define DWORD_SIZE 4
#define BMP_VALID_BIT_COUNT 24
#define BMP_RESERVED 0
#define BMP_HEADER_SIZE 40
#define BMP_PIXEL_PER_METER 2834
#define BMP_PLANES 1
#define BMP_CIR_USED 0
#define BMP_CIR_IMPORTANT 0
#define BMP_COMPRESSION 0


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_OUT_OF_MEMORY
    /* коды других ошибок  */
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};
enum read_status from_bmp( FILE const* in, struct image* img);
enum write_status to_bmp( FILE const* out, struct image const* img );
#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
