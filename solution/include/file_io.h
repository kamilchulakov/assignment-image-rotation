#ifndef ASSIGNMENT_IMAGE_ROTATION_LAB_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_LAB_IO_H
#include <stdio.h>
enum io_status {
    OK = 0,
    BAD = 1
};
FILE* open_read(const char* path);
FILE* open_write(const char* path);
enum io_status close(FILE* file);
#endif //ASSIGNMENT_IMAGE_ROTATION_LAB_IO_H
