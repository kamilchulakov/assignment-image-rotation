#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <inttypes.h>
#include <stdlib.h>
struct pixel { uint8_t b, g, r; };
struct image {
    size_t width, height;
    struct pixel* data;
};
struct image* image_create( size_t width, size_t height );
void image_free(struct image* img);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
