//
// Created by user on 24.11.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
#include "image.h"
#include <stdio.h>
/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image* rotated( struct image const* source );
#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
